<?php

namespace Fantassin\Typography;

use Fantassin\Core\HasHooks;

class SpacingRules implements HasHooks {

	public function hooks() {
		add_filter( 'the_content', [ $this, 'remove_multiple_spacing' ], 998 );
		add_filter( 'the_content', [ $this, 'add_non_breaking_space' ], 999 );
	}

	/**
	 * @param string $content
	 *
	 * @return string
	 */
	public function add_non_breaking_space( string $content ): string {
		$delimiters_before = [
			';',
			'?',
			'!',
//      ':', // TODO: exceptions for http: or https:
			'€',
			'»'
		];

		$delimiters_after = [
			'«'
		];

		foreach ( $delimiters_before as $delimiter ) {
			$content = $this->replace_regular_space_by_non_breaking_space( $content, $delimiter, true, false );
		}

		foreach ( $delimiters_after as $delimiter ) {
			$content = $this->replace_regular_space_by_non_breaking_space( $content, $delimiter, false, true );
		}

		$content = $this->remove_mixed_double_spacing( $content );

		return $content;
	}

	private function remove_mixed_double_spacing( string $content ): string {
		$content = str_replace( '&nbsp; ', '&nbsp;', $content );
		$content = str_replace( ' &nbsp;', '&nbsp;', $content );

		return $content;
	}

	private function replace_regular_space_by_non_breaking_space( string $content, string $delimiter, bool $is_before = false, bool $is_after = false ): string {

		$dom = new \DOMDocument;
		$dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );
		$xpath = new \DOMXPath( $dom );

		$before  = '';
		$after   = '';
		$replace = '';
		$nbsp = "\xc2\xa0";

		if ( $is_before ) {
			$before  = ' ';
			$replace .= $nbsp;
		}

		$replace .= $delimiter;

		if ( $is_after ) {
			$after   = ' ';
			$replace .= $nbsp;
		}

		foreach ( $xpath->query( '//text()' ) as $node ) {
			/**
			 * Force space around delimiter
			 */
			$node->textContent = $this->force_space_around_delimiter( $node->textContent, $delimiter, $before, $after );
			/**
			 * Replace space(s) around by non-breaking space(s)
			 */
			$node->textContent = str_replace( $before . $delimiter . $after, $replace, $node->nodeValue );
		}

		return $dom->saveHTML();
	}

	/**
	 * @param string $content
	 *
	 * @return string
	 */
	public function remove_multiple_spacing( string $content ): string {

		$dom = new \DOMDocument;
		$dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );
		$xpath = new \DOMXPath( $dom );

		foreach ( $xpath->query( '//text()' ) as $node ) {
			/**
			 * Replace $nbsp; to regular space
			 */
			$node->textContent = preg_replace( '/\xc2\xa0/', ' ', $node->textContent );
			/**
			 * Remove multiple spacing
			 */
			$node->textContent = preg_replace( '/\s+/', ' ', $node->textContent );
		}

		return $dom->saveHTML();
	}

	private function force_space_around_delimiter( string $content = '', string $delimiter = '', string $before = '', string $after = '' ) {
		$explode_content       = explode( $delimiter, $content );
		$explode_content_count = count( $explode_content );
		$content               = '';
		$i                     = 0;
		foreach ( $explode_content as $explode_part ) {
			$content .= $explode_part;
			if ( $i < $explode_content_count - 1 ) {
				$content .= $before . $delimiter . $after;
			}
			$i ++;
		}

		return $content;
	}
}
