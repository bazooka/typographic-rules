## Features
* Remove multiple spaces
* Add non-breaking spaces before `;`, `?`, `!`, `€`, `»`
* Add non-breaking spaces after `«`

## Roadmap
* Add non breaking spaces before `:` with some protocol exceptions `http://` or `https://`
